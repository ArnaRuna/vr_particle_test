// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_Grabber.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_Grabber() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UEnum* Z_Construct_UEnum_RunebergVRPlugin_EGrabTypeEnum();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Grabber_Grab();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Grabber();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Grabber_GrabSun();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Grabber_PullGrabbedObject();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Grabber_PushGrabbedObject();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Grabber_Release();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Grabber_SetDistanceFromController();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Grabber_StopPull();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Grabber_StopPush();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Grabber_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
static UEnum* EGrabTypeEnum_StaticEnum()
{
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		Singleton = GetStaticEnum(Z_Construct_UEnum_RunebergVRPlugin_EGrabTypeEnum, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("EGrabTypeEnum"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGrabTypeEnum(EGrabTypeEnum_StaticEnum, TEXT("/Script/RunebergVRPlugin"), TEXT("EGrabTypeEnum"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_RunebergVRPlugin_EGrabTypeEnum()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UEnum_RunebergVRPlugin_EGrabTypeEnum_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGrabTypeEnum"), 0, Get_Z_Construct_UEnum_RunebergVRPlugin_EGrabTypeEnum_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EGrabTypeEnum"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EGrabTypeEnum::PRECISION_GRAB"), 0);
			EnumNames.Emplace(TEXT("EGrabTypeEnum::SNAP_GRAB"), 1);
			EnumNames.Emplace(TEXT("EGrabTypeEnum::LOCK_GRAB"), 2);
			EnumNames.Emplace(TEXT("EGrabTypeEnum::DANGLING_GRAB"), 3);
			EnumNames.Emplace(TEXT("EGrabTypeEnum::PRECISION_LOCK"), 4);
			EnumNames.Emplace(TEXT("EGrabTypeEnum::EGrabTypeEnum_MAX"), 5);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EGrabTypeEnum");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("DANGLING_GRAB.DisplayName"), TEXT("Precision Grab and Dangle"));
			MetaData->SetValue(ReturnEnum, TEXT("LOCK_GRAB.DisplayName"), TEXT("Locked Rotation Grab"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
			MetaData->SetValue(ReturnEnum, TEXT("PRECISION_GRAB.DisplayName"), TEXT("Precision Grab"));
			MetaData->SetValue(ReturnEnum, TEXT("PRECISION_LOCK.DisplayName"), TEXT("Precision Grab with Locked Rotation"));
			MetaData->SetValue(ReturnEnum, TEXT("SNAP_GRAB.DisplayName"), TEXT("Snap to Mesh Origin Grab"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_RunebergVRPlugin_EGrabTypeEnum_CRC() { return 664044129U; }
	void URunebergVR_Grabber::StaticRegisterNativesURunebergVR_Grabber()
	{
		UClass* Class = URunebergVR_Grabber::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "Grab", (Native)&URunebergVR_Grabber::execGrab },
			{ "GrabSun", (Native)&URunebergVR_Grabber::execGrabSun },
			{ "PullGrabbedObject", (Native)&URunebergVR_Grabber::execPullGrabbedObject },
			{ "PushGrabbedObject", (Native)&URunebergVR_Grabber::execPushGrabbedObject },
			{ "Release", (Native)&URunebergVR_Grabber::execRelease },
			{ "SetDistanceFromController", (Native)&URunebergVR_Grabber::execSetDistanceFromController },
			{ "StopPull", (Native)&URunebergVR_Grabber::execStopPull },
			{ "StopPush", (Native)&URunebergVR_Grabber::execStopPush },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Grabber_Grab()
	{
		struct RunebergVR_Grabber_eventGrab_Parms
		{
			float Reach;
			bool ScanOnlyWillManuallyAttach;
			EGrabTypeEnum GrabMode;
			FName TagName;
			FRotator Rotation_Offset;
			bool RetainObjectRotation;
			bool RetainDistance;
			bool ShowDebugLine;
			AActor* ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Grabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("Grab"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_Grabber_eventGrab_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReturnValue, RunebergVR_Grabber_eventGrab_Parms), 0x0010000000000580, Z_Construct_UClass_AActor_NoRegister());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ShowDebugLine, RunebergVR_Grabber_eventGrab_Parms);
			UProperty* NewProp_ShowDebugLine = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ShowDebugLine"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ShowDebugLine, RunebergVR_Grabber_eventGrab_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(ShowDebugLine, RunebergVR_Grabber_eventGrab_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(RetainDistance, RunebergVR_Grabber_eventGrab_Parms);
			UProperty* NewProp_RetainDistance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("RetainDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(RetainDistance, RunebergVR_Grabber_eventGrab_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(RetainDistance, RunebergVR_Grabber_eventGrab_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(RetainObjectRotation, RunebergVR_Grabber_eventGrab_Parms);
			UProperty* NewProp_RetainObjectRotation = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("RetainObjectRotation"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(RetainObjectRotation, RunebergVR_Grabber_eventGrab_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(RetainObjectRotation, RunebergVR_Grabber_eventGrab_Parms), sizeof(bool), true);
			UProperty* NewProp_Rotation_Offset = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Rotation_Offset"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(Rotation_Offset, RunebergVR_Grabber_eventGrab_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FRotator());
			UProperty* NewProp_TagName = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("TagName"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(TagName, RunebergVR_Grabber_eventGrab_Parms), 0x0010000000000080);
			UProperty* NewProp_GrabMode = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("GrabMode"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(GrabMode, RunebergVR_Grabber_eventGrab_Parms), 0x0010000000000080, Z_Construct_UEnum_RunebergVRPlugin_EGrabTypeEnum());
			UProperty* NewProp_GrabMode_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_GrabMode, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ScanOnlyWillManuallyAttach, RunebergVR_Grabber_eventGrab_Parms);
			UProperty* NewProp_ScanOnlyWillManuallyAttach = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ScanOnlyWillManuallyAttach"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ScanOnlyWillManuallyAttach, RunebergVR_Grabber_eventGrab_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(ScanOnlyWillManuallyAttach, RunebergVR_Grabber_eventGrab_Parms), sizeof(bool), true);
			UProperty* NewProp_Reach = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Reach"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Reach, RunebergVR_Grabber_eventGrab_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_GrabMode"), TEXT("PRECISION_GRAB"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Reach"), TEXT("5.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_RetainDistance"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_RetainObjectRotation"), TEXT("true"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_ScanOnlyWillManuallyAttach"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_ShowDebugLine"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Grab something within line trace range of controller"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Grabber_GrabSun()
	{
		struct RunebergVR_Grabber_eventGrabSun_Parms
		{
			AActor* Sky_Sphere;
			float SunCycleRate;
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Grabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GrabSun"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Grabber_eventGrabSun_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Grabber_eventGrabSun_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Grabber_eventGrabSun_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Grabber_eventGrabSun_Parms), sizeof(bool), true);
			UProperty* NewProp_SunCycleRate = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("SunCycleRate"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(SunCycleRate, RunebergVR_Grabber_eventGrabSun_Parms), 0x0010000000000080);
			UProperty* NewProp_Sky_Sphere = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Sky_Sphere"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Sky_Sphere, RunebergVR_Grabber_eventGrabSun_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_SunCycleRate"), TEXT("2.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Cycle World Day/Night"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Grabber_PullGrabbedObject()
	{
		struct RunebergVR_Grabber_eventPullGrabbedObject_Parms
		{
			float PullSpeed;
			float MinDistance;
			float MaxDistance;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Grabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PullGrabbedObject"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Grabber_eventPullGrabbedObject_Parms));
			UProperty* NewProp_MaxDistance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MaxDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MaxDistance, RunebergVR_Grabber_eventPullGrabbedObject_Parms), 0x0010000000000080);
			UProperty* NewProp_MinDistance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MinDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MinDistance, RunebergVR_Grabber_eventPullGrabbedObject_Parms), 0x0010000000000080);
			UProperty* NewProp_PullSpeed = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("PullSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(PullSpeed, RunebergVR_Grabber_eventPullGrabbedObject_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MaxDistance"), TEXT("20.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MinDistance"), TEXT("1.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_PullSpeed"), TEXT("1.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Pull grabbed object"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Grabber_PushGrabbedObject()
	{
		struct RunebergVR_Grabber_eventPushGrabbedObject_Parms
		{
			float PushSpeed;
			float MinDistance;
			float MaxDistance;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Grabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PushGrabbedObject"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Grabber_eventPushGrabbedObject_Parms));
			UProperty* NewProp_MaxDistance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MaxDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MaxDistance, RunebergVR_Grabber_eventPushGrabbedObject_Parms), 0x0010000000000080);
			UProperty* NewProp_MinDistance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MinDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MinDistance, RunebergVR_Grabber_eventPushGrabbedObject_Parms), 0x0010000000000080);
			UProperty* NewProp_PushSpeed = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("PushSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(PushSpeed, RunebergVR_Grabber_eventPushGrabbedObject_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MaxDistance"), TEXT("20.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MinDistance"), TEXT("1.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_PushSpeed"), TEXT("1.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Push grabbed object(s)"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Grabber_Release()
	{
		struct RunebergVR_Grabber_eventRelease_Parms
		{
			AActor* ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Grabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("Release"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Grabber_eventRelease_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReturnValue, RunebergVR_Grabber_eventRelease_Parms), 0x0010000000000580, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Release grabbed object"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Grabber_SetDistanceFromController()
	{
		struct RunebergVR_Grabber_eventSetDistanceFromController_Parms
		{
			float NewDistance;
			float MinDistance;
			float MaxDistance;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Grabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SetDistanceFromController"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Grabber_eventSetDistanceFromController_Parms));
			UProperty* NewProp_MaxDistance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MaxDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MaxDistance, RunebergVR_Grabber_eventSetDistanceFromController_Parms), 0x0010000000000080);
			UProperty* NewProp_MinDistance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MinDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MinDistance, RunebergVR_Grabber_eventSetDistanceFromController_Parms), 0x0010000000000080);
			UProperty* NewProp_NewDistance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(NewDistance, RunebergVR_Grabber_eventSetDistanceFromController_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Set distance from controller"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Grabber_StopPull()
	{
		struct RunebergVR_Grabber_eventStopPull_Parms
		{
			AActor* ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Grabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("StopPull"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Grabber_eventStopPull_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReturnValue, RunebergVR_Grabber_eventStopPull_Parms), 0x0010000000000580, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Stop Pull"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Grabber_StopPush()
	{
		struct RunebergVR_Grabber_eventStopPush_Parms
		{
			AActor* ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Grabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("StopPush"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Grabber_eventStopPush_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReturnValue, RunebergVR_Grabber_eventStopPush_Parms), 0x0010000000000580, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Stop Push"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URunebergVR_Grabber_NoRegister()
	{
		return URunebergVR_Grabber::StaticClass();
	}
	UClass* Z_Construct_UClass_URunebergVR_Grabber()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_USceneComponent();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = URunebergVR_Grabber::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20B00080u;

				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Grabber_Grab());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Grabber_GrabSun());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Grabber_PullGrabbedObject());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Grabber_PushGrabbedObject());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Grabber_Release());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Grabber_SetDistanceFromController());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Grabber_StopPull());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Grabber_StopPush());

				UProperty* NewProp_SunBrightness = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SunBrightness"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(SunBrightness, URunebergVR_Grabber), 0x0010000000000005);
				UProperty* NewProp_HorizonPitch = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("HorizonPitch"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(HorizonPitch, URunebergVR_Grabber), 0x0010000000000005);
				UProperty* NewProp_SunReferencePoint = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SunReferencePoint"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(SunReferencePoint, URunebergVR_Grabber), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_MaxDistanceFromController = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MaxDistanceFromController"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MaxDistanceFromController, URunebergVR_Grabber), 0x0010000000000005);
				UProperty* NewProp_MinDistanceFromController = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MinDistanceFromController"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MinDistanceFromController, URunebergVR_Grabber), 0x0010000000000005);
				UProperty* NewProp_DistanceFromController = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DistanceFromController"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(DistanceFromController, URunebergVR_Grabber), 0x0010000000020015);
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Grabber_Grab(), "Grab"); // 307498088
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Grabber_GrabSun(), "GrabSun"); // 2186216733
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Grabber_PullGrabbedObject(), "PullGrabbedObject"); // 3158900108
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Grabber_PushGrabbedObject(), "PushGrabbedObject"); // 850746477
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Grabber_Release(), "Release"); // 1121150048
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Grabber_SetDistanceFromController(), "SetDistanceFromController"); // 817982183
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Grabber_StopPull(), "StopPull"); // 82134718
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Grabber_StopPush(), "StopPush"); // 4286162378
				static TCppClassTypeInfo<TCppClassTypeTraits<URunebergVR_Grabber> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("VR"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Trigger PhysicsVolume"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_Grabber.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
				MetaData->SetValue(NewProp_SunBrightness, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_SunBrightness, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
				MetaData->SetValue(NewProp_SunBrightness, TEXT("ToolTip"), TEXT("The normal sun brightness - leave as default if using built-in skysphere"));
				MetaData->SetValue(NewProp_HorizonPitch, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_HorizonPitch, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
				MetaData->SetValue(NewProp_HorizonPitch, TEXT("ToolTip"), TEXT("The horizon pitch - moving beyond this point we should turn off the sun brightness for the day-night cycle mechanic -leave as default if using built-in skysphere"));
				MetaData->SetValue(NewProp_SunReferencePoint, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_SunReferencePoint, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
				MetaData->SetValue(NewProp_SunReferencePoint, TEXT("ToolTip"), TEXT("The westmost point of this world for the day-night cycle mechanic -leave as default if using built-in skysphere"));
				MetaData->SetValue(NewProp_MaxDistanceFromController, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_MaxDistanceFromController, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
				MetaData->SetValue(NewProp_MaxDistanceFromController, TEXT("ToolTip"), TEXT("Min & Max Distance for Controller for grabbed objects  - Customize Push & Pull functions mid action"));
				MetaData->SetValue(NewProp_MinDistanceFromController, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_MinDistanceFromController, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
				MetaData->SetValue(NewProp_MinDistanceFromController, TEXT("ToolTip"), TEXT("Min Distance for Controller for grabbed objects  - Customize Push & Pull functions mid action"));
				MetaData->SetValue(NewProp_DistanceFromController, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_DistanceFromController, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Grabber.h"));
				MetaData->SetValue(NewProp_DistanceFromController, TEXT("ToolTip"), TEXT("Current Distance of grabbed items from their respective controllers"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(URunebergVR_Grabber, 1456471345);
	static FCompiledInDefer Z_CompiledInDefer_UClass_URunebergVR_Grabber(Z_Construct_UClass_URunebergVR_Grabber, &URunebergVR_Grabber::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("URunebergVR_Grabber"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URunebergVR_Grabber);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
