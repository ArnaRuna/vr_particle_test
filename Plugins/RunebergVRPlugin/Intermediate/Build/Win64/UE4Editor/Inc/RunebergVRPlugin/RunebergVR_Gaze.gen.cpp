// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_Gaze.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_Gaze() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UDelegateFunction_RunebergVRPlugin_ComponentGazeLostSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UDelegateFunction_RunebergVRPlugin_ComponentGazeActivateSignature__DelegateSignature();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UDelegateFunction_RunebergVRPlugin_ComponentGazeHasHitSignature__DelegateSignature();
	RUNEBERGVRPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FFrontGaze();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECollisionChannel();
	RUNEBERGVRPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FGazeReadOnly();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Gaze_EndGaze();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Gaze();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Gaze_StartGaze();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Gaze_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
// End Cross Module References
	UFunction* Z_Construct_UDelegateFunction_RunebergVRPlugin_ComponentGazeLostSignature__DelegateSignature()
	{
		struct _Script_RunebergVRPlugin_eventComponentGazeLostSignature_Parms
		{
			FHitResult LastGazeHit;
		};
		UObject* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ComponentGazeLostSignature__DelegateSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UDelegateFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00130000, 65535, sizeof(_Script_RunebergVRPlugin_eventComponentGazeLostSignature_Parms));
			UProperty* NewProp_LastGazeHit = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LastGazeHit"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(LastGazeHit, _Script_RunebergVRPlugin_eventComponentGazeLostSignature_Parms), 0x0010008000000080, Z_Construct_UScriptStruct_FHitResult());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UDelegateFunction_RunebergVRPlugin_ComponentGazeActivateSignature__DelegateSignature()
	{
		struct _Script_RunebergVRPlugin_eventComponentGazeActivateSignature_Parms
		{
			FHitResult GazeHit;
		};
		UObject* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ComponentGazeActivateSignature__DelegateSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UDelegateFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00130000, 65535, sizeof(_Script_RunebergVRPlugin_eventComponentGazeActivateSignature_Parms));
			UProperty* NewProp_GazeHit = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("GazeHit"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(GazeHit, _Script_RunebergVRPlugin_eventComponentGazeActivateSignature_Parms), 0x0010008000000080, Z_Construct_UScriptStruct_FHitResult());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UDelegateFunction_RunebergVRPlugin_ComponentGazeHasHitSignature__DelegateSignature()
	{
		struct _Script_RunebergVRPlugin_eventComponentGazeHasHitSignature_Parms
		{
			FHitResult GazeHit;
			float PercentActive;
		};
		UObject* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ComponentGazeHasHitSignature__DelegateSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UDelegateFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00130000, 65535, sizeof(_Script_RunebergVRPlugin_eventComponentGazeHasHitSignature_Parms));
			UProperty* NewProp_PercentActive = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("PercentActive"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(PercentActive, _Script_RunebergVRPlugin_eventComponentGazeHasHitSignature_Parms), 0x0010000000000080);
			UProperty* NewProp_GazeHit = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("GazeHit"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(GazeHit, _Script_RunebergVRPlugin_eventComponentGazeHasHitSignature_Parms), 0x0010008000000080, Z_Construct_UScriptStruct_FHitResult());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Delegates"));
#endif
		}
		return ReturnFunction;
	}
class UScriptStruct* FFrontGaze::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RUNEBERGVRPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FFrontGaze_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFrontGaze, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("FrontGaze"), sizeof(FFrontGaze), Get_Z_Construct_UScriptStruct_FFrontGaze_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFrontGaze(FFrontGaze::StaticStruct, TEXT("/Script/RunebergVRPlugin"), TEXT("FrontGaze"), false, nullptr, nullptr);
static struct FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFFrontGaze
{
	FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFFrontGaze()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("FrontGaze")),new UScriptStruct::TCppStructOps<FFrontGaze>);
	}
} ScriptStruct_RunebergVRPlugin_StaticRegisterNativesFFrontGaze;
	UScriptStruct* Z_Construct_UScriptStruct_FFrontGaze()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FFrontGaze_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FrontGaze"), sizeof(FFrontGaze), Get_Z_Construct_UScriptStruct_FFrontGaze_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("FrontGaze"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FFrontGaze>, EStructFlags(0x00000001));
			UProperty* NewProp_TargetScale3D = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("TargetScale3D"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(TargetScale3D, FFrontGaze), 0x0010000000000001, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_TargetRotation = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("TargetRotation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(TargetRotation, FFrontGaze), 0x0010000000000001, Z_Construct_UScriptStruct_FRotator());
			UProperty* NewProp_TargetMaterial = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("TargetMaterial"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(TargetMaterial, FFrontGaze), 0x0010000000000001, Z_Construct_UClass_UMaterialInterface_NoRegister());
			UProperty* NewProp_TargetStaticMesh = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("TargetStaticMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(TargetStaticMesh, FFrontGaze), 0x0010000000000001, Z_Construct_UClass_UStaticMesh_NoRegister());
			UProperty* NewProp_TargetCollisionType = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("TargetCollisionType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(CPP_PROPERTY_BASE(TargetCollisionType, FFrontGaze), 0x0010000000000001, Z_Construct_UEnum_Engine_ECollisionChannel());
			UProperty* NewProp_TargetTag = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("TargetTag"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(TargetTag, FFrontGaze), 0x0010000000000001);
			UProperty* NewProp_GazeCurrentDuration = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("GazeCurrentDuration"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(GazeCurrentDuration, FFrontGaze), 0x0010000000000001);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(StopGazeAfterHit, FFrontGaze);
			UProperty* NewProp_StopGazeAfterHit = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("StopGazeAfterHit"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(StopGazeAfterHit, FFrontGaze), 0x0010000000000001, CPP_BOOL_PROPERTY_BITMASK(StopGazeAfterHit, FFrontGaze), sizeof(bool), true);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(ReturnStruct, TEXT("ToolTip"), TEXT("Front Gaze variables"));
			MetaData->SetValue(NewProp_TargetScale3D, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_TargetScale3D, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(NewProp_TargetScale3D, TEXT("ToolTip"), TEXT("Scale of Target mesh"));
			MetaData->SetValue(NewProp_TargetRotation, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_TargetRotation, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(NewProp_TargetRotation, TEXT("ToolTip"), TEXT("Rotation of Target mesh"));
			MetaData->SetValue(NewProp_TargetMaterial, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_TargetMaterial, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(NewProp_TargetMaterial, TEXT("ToolTip"), TEXT("Target Material to apply to the static mesh"));
			MetaData->SetValue(NewProp_TargetStaticMesh, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_TargetStaticMesh, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(NewProp_TargetStaticMesh, TEXT("ToolTip"), TEXT("Target Static Mesh"));
			MetaData->SetValue(NewProp_TargetCollisionType, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_TargetCollisionType, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(NewProp_TargetCollisionType, TEXT("ToolTip"), TEXT("Collision type to check for"));
			MetaData->SetValue(NewProp_TargetTag, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_TargetTag, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(NewProp_TargetTag, TEXT("ToolTip"), TEXT("Actor tag to check for in target(s)"));
			MetaData->SetValue(NewProp_GazeCurrentDuration, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_GazeCurrentDuration, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(NewProp_GazeCurrentDuration, TEXT("ToolTip"), TEXT("How long do you need to gaze"));
			MetaData->SetValue(NewProp_StopGazeAfterHit, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_StopGazeAfterHit, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(NewProp_StopGazeAfterHit, TEXT("ToolTip"), TEXT("Whether or not gaze mode is on"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFrontGaze_CRC() { return 1576909986U; }
class UScriptStruct* FGazeReadOnly::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RUNEBERGVRPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FGazeReadOnly_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGazeReadOnly, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("GazeReadOnly"), sizeof(FGazeReadOnly), Get_Z_Construct_UScriptStruct_FGazeReadOnly_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGazeReadOnly(FGazeReadOnly::StaticStruct, TEXT("/Script/RunebergVRPlugin"), TEXT("GazeReadOnly"), false, nullptr, nullptr);
static struct FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFGazeReadOnly
{
	FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFGazeReadOnly()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("GazeReadOnly")),new UScriptStruct::TCppStructOps<FGazeReadOnly>);
	}
} ScriptStruct_RunebergVRPlugin_StaticRegisterNativesFGazeReadOnly;
	UScriptStruct* Z_Construct_UScriptStruct_FGazeReadOnly()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FGazeReadOnly_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GazeReadOnly"), sizeof(FGazeReadOnly), Get_Z_Construct_UScriptStruct_FGazeReadOnly_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GazeReadOnly"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FGazeReadOnly>, EStructFlags(0x00000001));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(GazeHasHit, FGazeReadOnly);
			UProperty* NewProp_GazeHasHit = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("GazeHasHit"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(GazeHasHit, FGazeReadOnly), 0x0010000000000001, CPP_BOOL_PROPERTY_BITMASK(GazeHasHit, FGazeReadOnly), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(IsGazing, FGazeReadOnly);
			UProperty* NewProp_IsGazing = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("IsGazing"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(IsGazing, FGazeReadOnly), 0x0010000000000001, CPP_BOOL_PROPERTY_BITMASK(IsGazing, FGazeReadOnly), sizeof(bool), true);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(ReturnStruct, TEXT("ToolTip"), TEXT("Read only variables"));
			MetaData->SetValue(NewProp_GazeHasHit, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_GazeHasHit, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(NewProp_GazeHasHit, TEXT("ToolTip"), TEXT("Gaze has hit"));
			MetaData->SetValue(NewProp_IsGazing, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_IsGazing, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(NewProp_IsGazing, TEXT("ToolTip"), TEXT("Whether or not gaze mode is on"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGazeReadOnly_CRC() { return 606199899U; }
	void URunebergVR_Gaze::StaticRegisterNativesURunebergVR_Gaze()
	{
		UClass* Class = URunebergVR_Gaze::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "EndGaze", (Native)&URunebergVR_Gaze::execEndGaze },
			{ "StartGaze", (Native)&URunebergVR_Gaze::execStartGaze },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Gaze_EndGaze()
	{
		UObject* Outer = Z_Construct_UClass_URunebergVR_Gaze();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EndGaze"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("End Gaze"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Gaze_StartGaze()
	{
		struct RunebergVR_Gaze_eventStartGaze_Parms
		{
			float Gaze_Range;
			float Gaze_TargetDuration;
			bool DrawDebugLine;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Gaze();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("StartGaze"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Gaze_eventStartGaze_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(DrawDebugLine, RunebergVR_Gaze_eventStartGaze_Parms);
			UProperty* NewProp_DrawDebugLine = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("DrawDebugLine"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(DrawDebugLine, RunebergVR_Gaze_eventStartGaze_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(DrawDebugLine, RunebergVR_Gaze_eventStartGaze_Parms), sizeof(bool), true);
			UProperty* NewProp_Gaze_TargetDuration = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Gaze_TargetDuration"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Gaze_TargetDuration, RunebergVR_Gaze_eventStartGaze_Parms), 0x0010000000000080);
			UProperty* NewProp_Gaze_Range = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Gaze_Range"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Gaze_Range, RunebergVR_Gaze_eventStartGaze_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_DrawDebugLine"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Gaze_Range"), TEXT("300.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Gaze_TargetDuration"), TEXT("3.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Start gaze"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URunebergVR_Gaze_NoRegister()
	{
		return URunebergVR_Gaze::StaticClass();
	}
	UClass* Z_Construct_UClass_URunebergVR_Gaze()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_USceneComponent();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = URunebergVR_Gaze::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20B00080u;

				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Gaze_EndGaze());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Gaze_StartGaze());

				UProperty* NewProp_OnGazeLost = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OnGazeLost"), RF_Public|RF_Transient|RF_MarkAsNative) UMulticastDelegateProperty(CPP_PROPERTY_BASE(OnGazeLost, URunebergVR_Gaze), 0x0010000010080000, Z_Construct_UDelegateFunction_RunebergVRPlugin_ComponentGazeLostSignature__DelegateSignature());
				UProperty* NewProp_OnGazeActivate = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OnGazeActivate"), RF_Public|RF_Transient|RF_MarkAsNative) UMulticastDelegateProperty(CPP_PROPERTY_BASE(OnGazeActivate, URunebergVR_Gaze), 0x0010000010080000, Z_Construct_UDelegateFunction_RunebergVRPlugin_ComponentGazeActivateSignature__DelegateSignature());
				UProperty* NewProp_OnGazeHit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OnGazeHit"), RF_Public|RF_Transient|RF_MarkAsNative) UMulticastDelegateProperty(CPP_PROPERTY_BASE(OnGazeHit, URunebergVR_Gaze), 0x0010000010080000, Z_Construct_UDelegateFunction_RunebergVRPlugin_ComponentGazeHasHitSignature__DelegateSignature());
				UProperty* NewProp_FrontGazeVariables = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FrontGazeVariables"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(FrontGazeVariables, URunebergVR_Gaze), 0x0010000000000005, Z_Construct_UScriptStruct_FFrontGaze());
				UProperty* NewProp_RuntimeReadOnly = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("RuntimeReadOnly"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(RuntimeReadOnly, URunebergVR_Gaze), 0x0010000000020015, Z_Construct_UScriptStruct_FGazeReadOnly());
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Gaze_EndGaze(), "EndGaze"); // 239638050
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Gaze_StartGaze(), "StartGaze"); // 3498866446
				static TCppClassTypeInfo<TCppClassTypeTraits<URunebergVR_Gaze> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("Custom"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Trigger PhysicsVolume"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_Gaze.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
				MetaData->SetValue(NewProp_OnGazeLost, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_OnGazeLost, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
				MetaData->SetValue(NewProp_OnGazeLost, TEXT("ToolTip"), TEXT("Blueprint event - When gaze ends"));
				MetaData->SetValue(NewProp_OnGazeActivate, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_OnGazeActivate, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
				MetaData->SetValue(NewProp_OnGazeActivate, TEXT("ToolTip"), TEXT("Blueprint event - When gaze activates action (duration met)"));
				MetaData->SetValue(NewProp_OnGazeHit, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_OnGazeHit, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
				MetaData->SetValue(NewProp_OnGazeHit, TEXT("ToolTip"), TEXT("Blueprint event - When gaze hits a valid object"));
				MetaData->SetValue(NewProp_FrontGazeVariables, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_FrontGazeVariables, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
				MetaData->SetValue(NewProp_FrontGazeVariables, TEXT("ToolTip"), TEXT("Front Gaze Variables"));
				MetaData->SetValue(NewProp_RuntimeReadOnly, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_RuntimeReadOnly, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gaze.h"));
				MetaData->SetValue(NewProp_RuntimeReadOnly, TEXT("ToolTip"), TEXT("Read only variables"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(URunebergVR_Gaze, 2448574570);
	static FCompiledInDefer Z_CompiledInDefer_UClass_URunebergVR_Gaze(Z_Construct_UClass_URunebergVR_Gaze, &URunebergVR_Gaze::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("URunebergVR_Gaze"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URunebergVR_Gaze);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
