// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_Gestures_Database.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_Gestures_Database() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FVRGesture();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Gestures_Database_NoRegister();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Gestures_Database();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
// End Cross Module References
class UScriptStruct* FVRGesture::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RUNEBERGVRPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FVRGesture_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FVRGesture, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("VRGesture"), sizeof(FVRGesture), Get_Z_Construct_UScriptStruct_FVRGesture_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FVRGesture(FVRGesture::StaticStruct, TEXT("/Script/RunebergVRPlugin"), TEXT("VRGesture"), false, nullptr, nullptr);
static struct FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFVRGesture
{
	FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFVRGesture()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("VRGesture")),new UScriptStruct::TCppStructOps<FVRGesture>);
	}
} ScriptStruct_RunebergVRPlugin_StaticRegisterNativesFVRGesture;
	UScriptStruct* Z_Construct_UScriptStruct_FVRGesture()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FVRGesture_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("VRGesture"), sizeof(FVRGesture), Get_Z_Construct_UScriptStruct_FVRGesture_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("VRGesture"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FVRGesture>, EStructFlags(0x00000001));
			UProperty* NewProp_GesturePattern = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("GesturePattern"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(GesturePattern, FVRGesture), 0x0010000000000005);
			UProperty* NewProp_GesturePattern_Inner = new(EC_InternalUseOnlyConstructor, NewProp_GesturePattern, TEXT("GesturePattern"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_GestureName = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("GestureName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(GestureName, FVRGesture), 0x0010000000000005);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures_Database.h"));
			MetaData->SetValue(NewProp_GesturePattern, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_GesturePattern, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures_Database.h"));
			MetaData->SetValue(NewProp_GestureName, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_GestureName, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures_Database.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FVRGesture_CRC() { return 2351036616U; }
	void URunebergVR_Gestures_Database::StaticRegisterNativesURunebergVR_Gestures_Database()
	{
	}
	UClass* Z_Construct_UClass_URunebergVR_Gestures_Database_NoRegister()
	{
		return URunebergVR_Gestures_Database::StaticClass();
	}
	UClass* Z_Construct_UClass_URunebergVR_Gestures_Database()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UDataAsset();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = URunebergVR_Gestures_Database::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20100080u;


				UProperty* NewProp_VRGesturePatterns = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("VRGesturePatterns"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(VRGesturePatterns, URunebergVR_Gestures_Database), 0x0010000000000005);
				UProperty* NewProp_VRGesturePatterns_Inner = new(EC_InternalUseOnlyConstructor, NewProp_VRGesturePatterns, TEXT("VRGesturePatterns"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UScriptStruct_FVRGesture());
				static TCppClassTypeInfo<TCppClassTypeTraits<URunebergVR_Gestures_Database> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintType"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_Gestures_Database.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures_Database.h"));
				MetaData->SetValue(OuterClass, TEXT("ToolTip"), TEXT("Gesture pattern database"));
				MetaData->SetValue(NewProp_VRGesturePatterns, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_VRGesturePatterns, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures_Database.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(URunebergVR_Gestures_Database, 2341526158);
	static FCompiledInDefer Z_CompiledInDefer_UClass_URunebergVR_Gestures_Database(Z_Construct_UClass_URunebergVR_Gestures_Database, &URunebergVR_Gestures_Database::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("URunebergVR_Gestures_Database"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URunebergVR_Gestures_Database);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
