// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_Teleporter.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_Teleporter() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UEnum* Z_Construct_UEnum_RunebergVRPlugin_EMoveDirectionEnum();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_HideMarker();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Teleporter();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_HideTeleportArc();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_HideTeleportRay();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_MoveMarker();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_ShowMarker();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_ShowTeleportArc();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_ShowTeleportRay();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_TeleportNow();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Teleporter_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
// End Cross Module References
static UEnum* EMoveDirectionEnum_StaticEnum()
{
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		Singleton = GetStaticEnum(Z_Construct_UEnum_RunebergVRPlugin_EMoveDirectionEnum, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("EMoveDirectionEnum"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMoveDirectionEnum(EMoveDirectionEnum_StaticEnum, TEXT("/Script/RunebergVRPlugin"), TEXT("EMoveDirectionEnum"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_RunebergVRPlugin_EMoveDirectionEnum()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UEnum_RunebergVRPlugin_EMoveDirectionEnum_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMoveDirectionEnum"), 0, Get_Z_Construct_UEnum_RunebergVRPlugin_EMoveDirectionEnum_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EMoveDirectionEnum"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EMoveDirectionEnum::MOVE_FORWARD"), 0);
			EnumNames.Emplace(TEXT("EMoveDirectionEnum::MOVE_BACKWARD"), 1);
			EnumNames.Emplace(TEXT("EMoveDirectionEnum::MOVE_LEFT"), 2);
			EnumNames.Emplace(TEXT("EMoveDirectionEnum::MOVE_RIGHT"), 3);
			EnumNames.Emplace(TEXT("EMoveDirectionEnum::MOVE_CUSTOM"), 4);
			EnumNames.Emplace(TEXT("EMoveDirectionEnum::MOVE_MAX"), 5);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EMoveDirectionEnum");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
			MetaData->SetValue(ReturnEnum, TEXT("MOVE_BACKWARD.DisplayName"), TEXT("Away from Player"));
			MetaData->SetValue(ReturnEnum, TEXT("MOVE_CUSTOM.DisplayName"), TEXT("Use a Custom Rotation for Direction"));
			MetaData->SetValue(ReturnEnum, TEXT("MOVE_FORWARD.DisplayName"), TEXT("Towards Player"));
			MetaData->SetValue(ReturnEnum, TEXT("MOVE_LEFT.DisplayName"), TEXT("Left of Player"));
			MetaData->SetValue(ReturnEnum, TEXT("MOVE_RIGHT.DisplayName"), TEXT("Right of Player"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_RunebergVRPlugin_EMoveDirectionEnum_CRC() { return 3708513853U; }
	void URunebergVR_Teleporter::StaticRegisterNativesURunebergVR_Teleporter()
	{
		UClass* Class = URunebergVR_Teleporter::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "HideMarker", (Native)&URunebergVR_Teleporter::execHideMarker },
			{ "HideTeleportArc", (Native)&URunebergVR_Teleporter::execHideTeleportArc },
			{ "HideTeleportRay", (Native)&URunebergVR_Teleporter::execHideTeleportRay },
			{ "MoveMarker", (Native)&URunebergVR_Teleporter::execMoveMarker },
			{ "ShowMarker", (Native)&URunebergVR_Teleporter::execShowMarker },
			{ "ShowTeleportArc", (Native)&URunebergVR_Teleporter::execShowTeleportArc },
			{ "ShowTeleportRay", (Native)&URunebergVR_Teleporter::execShowTeleportRay },
			{ "TeleportNow", (Native)&URunebergVR_Teleporter::execTeleportNow },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_HideMarker()
	{
		struct RunebergVR_Teleporter_eventHideMarker_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Teleporter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("HideMarker"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Teleporter_eventHideMarker_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Teleporter_eventHideMarker_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Teleporter_eventHideMarker_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Teleporter_eventHideMarker_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Remove marker"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_HideTeleportArc()
	{
		struct RunebergVR_Teleporter_eventHideTeleportArc_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Teleporter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("HideTeleportArc"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Teleporter_eventHideTeleportArc_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Teleporter_eventHideTeleportArc_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Teleporter_eventHideTeleportArc_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Teleporter_eventHideTeleportArc_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Remove the teleportation arc trace"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_HideTeleportRay()
	{
		struct RunebergVR_Teleporter_eventHideTeleportRay_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Teleporter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("HideTeleportRay"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Teleporter_eventHideTeleportRay_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Teleporter_eventHideTeleportRay_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Teleporter_eventHideTeleportRay_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Teleporter_eventHideTeleportRay_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Remove the teleportation ray trace"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_MoveMarker()
	{
		struct RunebergVR_Teleporter_eventMoveMarker_Parms
		{
			EMoveDirectionEnum MarkerDirection;
			int32 Rate;
			FRotator CustomDirection;
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Teleporter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("MoveMarker"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_Teleporter_eventMoveMarker_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Teleporter_eventMoveMarker_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Teleporter_eventMoveMarker_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Teleporter_eventMoveMarker_Parms), sizeof(bool), true);
			UProperty* NewProp_CustomDirection = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("CustomDirection"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(CustomDirection, RunebergVR_Teleporter_eventMoveMarker_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FRotator());
			UProperty* NewProp_Rate = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Rate"), RF_Public|RF_Transient|RF_MarkAsNative) UUnsizedIntProperty(CPP_PROPERTY_BASE(Rate, RunebergVR_Teleporter_eventMoveMarker_Parms), 0x0010000000000080);
			UProperty* NewProp_MarkerDirection = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MarkerDirection"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(MarkerDirection, RunebergVR_Teleporter_eventMoveMarker_Parms), 0x0010000000000080, Z_Construct_UEnum_RunebergVRPlugin_EMoveDirectionEnum());
			UProperty* NewProp_MarkerDirection_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_MarkerDirection, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MarkerDirection"), TEXT("MOVE_FORWARD"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Rate"), TEXT("25"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Move Marker"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_ShowMarker()
	{
		struct RunebergVR_Teleporter_eventShowMarker_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Teleporter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ShowMarker"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Teleporter_eventShowMarker_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Teleporter_eventShowMarker_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Teleporter_eventShowMarker_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Teleporter_eventShowMarker_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Show marker in the world"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_ShowTeleportArc()
	{
		struct RunebergVR_Teleporter_eventShowTeleportArc_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Teleporter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ShowTeleportArc"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Teleporter_eventShowTeleportArc_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Teleporter_eventShowTeleportArc_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Teleporter_eventShowTeleportArc_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Teleporter_eventShowTeleportArc_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Show the teleportation arc trace"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_ShowTeleportRay()
	{
		struct RunebergVR_Teleporter_eventShowTeleportRay_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Teleporter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ShowTeleportRay"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Teleporter_eventShowTeleportRay_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Teleporter_eventShowTeleportRay_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Teleporter_eventShowTeleportRay_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Teleporter_eventShowTeleportRay_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Show the teleportation ray trace"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Teleporter_TeleportNow()
	{
		struct RunebergVR_Teleporter_eventTeleportNow_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Teleporter();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("TeleportNow"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Teleporter_eventTeleportNow_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Teleporter_eventTeleportNow_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Teleporter_eventTeleportNow_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Teleporter_eventTeleportNow_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Teleport"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URunebergVR_Teleporter_NoRegister()
	{
		return URunebergVR_Teleporter::StaticClass();
	}
	UClass* Z_Construct_UClass_URunebergVR_Teleporter()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_USceneComponent();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = URunebergVR_Teleporter::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20B00080u;

				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Teleporter_HideMarker());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Teleporter_HideTeleportArc());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Teleporter_HideTeleportRay());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Teleporter_MoveMarker());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Teleporter_ShowMarker());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Teleporter_ShowTeleportArc());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Teleporter_ShowTeleportRay());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Teleporter_TeleportNow());

				CPP_BOOL_PROPERTY_BITMASK_STRUCT(IsTeleporting, URunebergVR_Teleporter);
				UProperty* NewProp_IsTeleporting = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IsTeleporting"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(IsTeleporting, URunebergVR_Teleporter), 0x0010000000020005, CPP_BOOL_PROPERTY_BITMASK(IsTeleporting, URunebergVR_Teleporter), sizeof(bool), true);
				UProperty* NewProp_OculusHeightOffset = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OculusHeightOffset"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(OculusHeightOffset, URunebergVR_Teleporter), 0x0010000000000005);
				UProperty* NewProp_SteamVRHeightOffset = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SteamVRHeightOffset"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(SteamVRHeightOffset, URunebergVR_Teleporter), 0x0010000000000005);
				UProperty* NewProp_TeleportTargetParticleSpawnOffset = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TeleportTargetParticleSpawnOffset"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(TeleportTargetParticleSpawnOffset, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_TeleportTargetParticleScale = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TeleportTargetParticleScale"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(TeleportTargetParticleScale, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_TeleportTargetParticle = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TeleportTargetParticle"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(TeleportTargetParticle, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UClass_UParticleSystem_NoRegister());
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bFaceMarkerRotation, URunebergVR_Teleporter);
				UProperty* NewProp_bFaceMarkerRotation = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bFaceMarkerRotation"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bFaceMarkerRotation, URunebergVR_Teleporter), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(bFaceMarkerRotation, URunebergVR_Teleporter), sizeof(bool), true);
				UProperty* NewProp_CustomMarkerRotation = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CustomMarkerRotation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(CustomMarkerRotation, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UScriptStruct_FRotator());
				UProperty* NewProp_TeleportTargetMeshSpawnOffset = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TeleportTargetMeshSpawnOffset"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(TeleportTargetMeshSpawnOffset, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_TeleportTargetMeshScale = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TeleportTargetMeshScale"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(TeleportTargetMeshScale, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_TeleportTargetMesh = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TeleportTargetMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(TeleportTargetMesh, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UClass_UStaticMesh_NoRegister());
				UProperty* NewProp_FloorIsAtZ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FloorIsAtZ"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(FloorIsAtZ, URunebergVR_Teleporter), 0x0010000000000005);
				UProperty* NewProp_TeleportTargetPawnSpawnOffset = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TeleportTargetPawnSpawnOffset"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(TeleportTargetPawnSpawnOffset, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_ArcOverrideGravity = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ArcOverrideGravity"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ArcOverrideGravity, URunebergVR_Teleporter), 0x0010000000000005);
				UProperty* NewProp_BeamHitNavMeshTolerance = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BeamHitNavMeshTolerance"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(BeamHitNavMeshTolerance, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_RayScaleRate = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("RayScaleRate"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(RayScaleRate, URunebergVR_Teleporter), 0x0010000000000005);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(RayInstantScale, URunebergVR_Teleporter);
				UProperty* NewProp_RayInstantScale = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("RayInstantScale"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(RayInstantScale, URunebergVR_Teleporter), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(RayInstantScale, URunebergVR_Teleporter), sizeof(bool), true);
				UProperty* NewProp_BeamLocationOffset = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BeamLocationOffset"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(BeamLocationOffset, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_BeamMagnitude = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BeamMagnitude"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(BeamMagnitude, URunebergVR_Teleporter), 0x0010000000000005);
				UProperty* NewProp_TeleportBeamMesh = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TeleportBeamMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(TeleportBeamMesh, URunebergVR_Teleporter), 0x0010000000000005, Z_Construct_UClass_UStaticMesh_NoRegister());
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Teleporter_HideMarker(), "HideMarker"); // 3870326659
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Teleporter_HideTeleportArc(), "HideTeleportArc"); // 2981958425
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Teleporter_HideTeleportRay(), "HideTeleportRay"); // 2033600052
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Teleporter_MoveMarker(), "MoveMarker"); // 3303542837
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Teleporter_ShowMarker(), "ShowMarker"); // 4187597590
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Teleporter_ShowTeleportArc(), "ShowTeleportArc"); // 3939927267
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Teleporter_ShowTeleportRay(), "ShowTeleportRay"); // 117966615
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Teleporter_TeleportNow(), "TeleportNow"); // 1268547782
				static TCppClassTypeInfo<TCppClassTypeTraits<URunebergVR_Teleporter> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("VR"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Trigger PhysicsVolume"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_Teleporter.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_IsTeleporting, TEXT("Category"), TEXT("VR - Read Only"));
				MetaData->SetValue(NewProp_IsTeleporting, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_IsTeleporting, TEXT("ToolTip"), TEXT("Check to see if an active teleport mode is turned on"));
				MetaData->SetValue(NewProp_OculusHeightOffset, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_OculusHeightOffset, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_OculusHeightOffset, TEXT("ToolTip"), TEXT("Oculus HMD Location Offset"));
				MetaData->SetValue(NewProp_SteamVRHeightOffset, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_SteamVRHeightOffset, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_SteamVRHeightOffset, TEXT("ToolTip"), TEXT("SteamVR HMD Location Offset"));
				MetaData->SetValue(NewProp_TeleportTargetParticleSpawnOffset, TEXT("Category"), TEXT("VR - Teleport Target Parameters"));
				MetaData->SetValue(NewProp_TeleportTargetParticleSpawnOffset, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_TeleportTargetParticleScale, TEXT("Category"), TEXT("VR - Teleport Target Parameters"));
				MetaData->SetValue(NewProp_TeleportTargetParticleScale, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_TeleportTargetParticle, TEXT("Category"), TEXT("VR - Teleport Target Parameters"));
				MetaData->SetValue(NewProp_TeleportTargetParticle, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_bFaceMarkerRotation, TEXT("Category"), TEXT("VR - Teleport Target Parameters"));
				MetaData->SetValue(NewProp_bFaceMarkerRotation, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_bFaceMarkerRotation, TEXT("ToolTip"), TEXT("If player should face marker rotation (use with Custom Marker Rotation)"));
				MetaData->SetValue(NewProp_CustomMarkerRotation, TEXT("Category"), TEXT("VR - Teleport Target Parameters"));
				MetaData->SetValue(NewProp_CustomMarkerRotation, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_CustomMarkerRotation, TEXT("ToolTip"), TEXT("Custom marker rotation (applied per frame)"));
				MetaData->SetValue(NewProp_TeleportTargetMeshSpawnOffset, TEXT("Category"), TEXT("VR - Teleport Target Parameters"));
				MetaData->SetValue(NewProp_TeleportTargetMeshSpawnOffset, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_TeleportTargetMeshScale, TEXT("Category"), TEXT("VR - Teleport Target Parameters"));
				MetaData->SetValue(NewProp_TeleportTargetMeshScale, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_TeleportTargetMesh, TEXT("Category"), TEXT("VR - Teleport Target Parameters"));
				MetaData->SetValue(NewProp_TeleportTargetMesh, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_FloorIsAtZ, TEXT("Category"), TEXT("VR - Teleport Target Parameters"));
				MetaData->SetValue(NewProp_FloorIsAtZ, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_TeleportTargetPawnSpawnOffset, TEXT("Category"), TEXT("VR - Teleport Target Parameters"));
				MetaData->SetValue(NewProp_TeleportTargetPawnSpawnOffset, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_TeleportTargetPawnSpawnOffset, TEXT("ToolTip"), TEXT("Additional offset of pawn (internal offsets are Steam: 112, Rift: 250)"));
				MetaData->SetValue(NewProp_ArcOverrideGravity, TEXT("Category"), TEXT("VR - Teleport Beam Parameters"));
				MetaData->SetValue(NewProp_ArcOverrideGravity, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_ArcOverrideGravity, TEXT("ToolTip"), TEXT("The teleport beam's custom gravity"));
				MetaData->SetValue(NewProp_BeamHitNavMeshTolerance, TEXT("Category"), TEXT("VR - Teleport Beam Parameters"));
				MetaData->SetValue(NewProp_BeamHitNavMeshTolerance, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_BeamHitNavMeshTolerance, TEXT("ToolTip"), TEXT("The teleport beam's navigation mesh tolerance - fine tune to fit your nav mesh bounds"));
				MetaData->SetValue(NewProp_RayScaleRate, TEXT("Category"), TEXT("VR - Teleport Beam Parameters"));
				MetaData->SetValue(NewProp_RayScaleRate, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_RayScaleRate, TEXT("ToolTip"), TEXT("How much the ray will scale up until it reaches target location"));
				MetaData->SetValue(NewProp_RayInstantScale, TEXT("Category"), TEXT("VR - Teleport Beam Parameters"));
				MetaData->SetValue(NewProp_RayInstantScale, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_RayInstantScale, TEXT("ToolTip"), TEXT("For ray type beam, ensure the lenth of the beam reaches target location instantenously. Uses RayScaleRate as base length unit"));
				MetaData->SetValue(NewProp_BeamLocationOffset, TEXT("Category"), TEXT("VR - Teleport Beam Parameters"));
				MetaData->SetValue(NewProp_BeamLocationOffset, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_BeamLocationOffset, TEXT("ToolTip"), TEXT("A location offset from the parent mesh origin where the teleport beam will start"));
				MetaData->SetValue(NewProp_BeamMagnitude, TEXT("Category"), TEXT("VR - Teleport Beam Parameters"));
				MetaData->SetValue(NewProp_BeamMagnitude, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_BeamMagnitude, TEXT("ToolTip"), TEXT("The teleport beam's Launch Velocity Magnitude - higher number increases range of teleport"));
				MetaData->SetValue(NewProp_TeleportBeamMesh, TEXT("Category"), TEXT("VR - Teleport Beam Parameters"));
				MetaData->SetValue(NewProp_TeleportBeamMesh, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Teleporter.h"));
				MetaData->SetValue(NewProp_TeleportBeamMesh, TEXT("ToolTip"), TEXT("The teleport beam's mesh"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(URunebergVR_Teleporter, 2956594896);
	static FCompiledInDefer Z_CompiledInDefer_UClass_URunebergVR_Teleporter(Z_Construct_UClass_URunebergVR_Teleporter, &URunebergVR_Teleporter::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("URunebergVR_Teleporter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URunebergVR_Teleporter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
