// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_Climb.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_Climb() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Climb_Climb();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Climb();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Climb_LetGo();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Climb_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_CustomGravity_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void URunebergVR_Climb::StaticRegisterNativesURunebergVR_Climb()
	{
		UClass* Class = URunebergVR_Climb::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "Climb", (Native)&URunebergVR_Climb::execClimb },
			{ "LetGo", (Native)&URunebergVR_Climb::execLetGo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Climb_Climb()
	{
		UObject* Outer = Z_Construct_UClass_URunebergVR_Climb();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("Climb"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Climb.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Enable climb mode"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Climb_LetGo()
	{
		UObject* Outer = Z_Construct_UClass_URunebergVR_Climb();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("LetGo"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Climb.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Deactivate climb mode"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URunebergVR_Climb_NoRegister()
	{
		return URunebergVR_Climb::StaticClass();
	}
	UClass* Z_Construct_UClass_URunebergVR_Climb()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_USceneComponent();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = URunebergVR_Climb::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20B00080u;

				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Climb_Climb());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Climb_LetGo());

				UProperty* NewProp_CustomGravity = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CustomGravity"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(CustomGravity, URunebergVR_Climb), 0x001000000008000d, Z_Construct_UClass_URunebergVR_CustomGravity_NoRegister());
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(IsClimbing, URunebergVR_Climb);
				UProperty* NewProp_IsClimbing = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IsClimbing"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(IsClimbing, URunebergVR_Climb), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(IsClimbing, URunebergVR_Climb), sizeof(bool), true);
				UProperty* NewProp_InitialLocation = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InitialLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(InitialLocation, URunebergVR_Climb), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_ClimbTags = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ClimbTags"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(ClimbTags, URunebergVR_Climb), 0x0010000000000005);
				UProperty* NewProp_ClimbTags_Inner = new(EC_InternalUseOnlyConstructor, NewProp_ClimbTags, TEXT("ClimbTags"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Climb_Climb(), "Climb"); // 2777098177
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Climb_LetGo(), "LetGo"); // 2297261154
				static TCppClassTypeInfo<TCppClassTypeTraits<URunebergVR_Climb> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("VR"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Trigger PhysicsVolume"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_Climb.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Climb.h"));
				MetaData->SetValue(NewProp_CustomGravity, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_CustomGravity, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CustomGravity, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Climb.h"));
				MetaData->SetValue(NewProp_CustomGravity, TEXT("ToolTip"), TEXT("Whether this actor is currently falling"));
				MetaData->SetValue(NewProp_IsClimbing, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_IsClimbing, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Climb.h"));
				MetaData->SetValue(NewProp_IsClimbing, TEXT("ToolTip"), TEXT("Whether this actor is currently falling"));
				MetaData->SetValue(NewProp_InitialLocation, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_InitialLocation, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Climb.h"));
				MetaData->SetValue(NewProp_InitialLocation, TEXT("ToolTip"), TEXT("Gravity origin when gravity direction is set to RELATIVE"));
				MetaData->SetValue(NewProp_ClimbTags, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_ClimbTags, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Climb.h"));
				MetaData->SetValue(NewProp_ClimbTags, TEXT("ToolTip"), TEXT("Tags that can be used to stop gravity"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(URunebergVR_Climb, 1850072357);
	static FCompiledInDefer Z_CompiledInDefer_UClass_URunebergVR_Climb(Z_Construct_UClass_URunebergVR_Climb, &URunebergVR_Climb::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("URunebergVR_Climb"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URunebergVR_Climb);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
